import random

from pyglet.gl import *
from pyglet import shapes


window = pyglet.window.Window(800, 600)
window.set_mouse_visible(False)
mouse_x = 0
mouse_y = 0


class Particle:
    """Each particle has coordinates, a color, a direction (technically, veclocity), acceleration and gravity"""
    def __init__(self, x, y, r, dir_x, dir_y, gravity):
        self.x = x
        self.y = y
        self.radius = r
        self.colorx = 255
        self.colory = 255
        self.colorz = 0
        self.directionx = dir_x
        self.directiony = dir_y
        self.acc = 0
        self.gravity = gravity


class ParticleEmit:
    def __init__(self):
        """The particle emitter has a list of particles and the option to set more particles to spawn at a time"""
        self.particles = []
        self.num_of_new_particles = 1
        self.color_change = 0

    def emit(self):
        if self.particles:
            self.delete_particles()

            # this commented section is for adding more particles in the add_particles method
            """self.num_of_new_particles = 1
            intensity = random.uniform(0, 1)
            if intensity < 0.1:
                print("adding more particles")
                self.num_of_new_particles = 2"""

            for particle in self.particles:
                """Calculates new positions based on velocity/acc/gravity, draws the stars and changes colors."""
                particle.acc += particle.gravity
                particle.directionx += particle.acc
                particle.directiony += particle.acc

                particle.x += particle.directionx  # y koord
                particle.y += particle.directiony  # x koord
                particle.radius -= 0.2
                particle.acc = 0

                star = shapes.Star(x=particle.x, y=particle.y, outer_radius=particle.radius,
                                   inner_radius=particle.radius - 5, num_spikes=5, rotation=0,
                                   color=(particle.colorx, particle.colory, particle.colorz), batch=None, group=None)

                star.draw()
                particle.colorz += 10
                particle.colory -= 10

    def add_particles(self):
        """Makes new particles with random velocity"""
        global mouse_x, mouse_y
        radius = 10
        direction_x = random.randint(-2, 2)
        direction_y = random.randint(-2, 4)
        gravity = -0.1
        new_particle = Particle(mouse_x, mouse_y, radius, direction_x, direction_y, gravity)
        for n in range(self.num_of_new_particles):
            self.particles.append(new_particle)

    def delete_particles(self):
        """Deletes old particles. In this case, it keeps particles that still have a radius above zero."""
        particle_copy = [particle for particle in self.particles if particle.radius > 0]
        self.particles = particle_copy


particle_sys = ParticleEmit()


def update(*args):
    pass


@window.event
def on_mouse_motion(x, y, dx, dy):
    """Tracks the motion of the mouse. Positions the sprite (wand) to align with the emitting point"""
    global mouse_x, mouse_y
    mouse_x = x
    mouse_y = y

    sprite.x = x
    sprite.y = y-100


wand = pyglet.image.load('wand2.png')
sprite = pyglet.sprite.Sprite(wand, x=250, y=250)
sprite.scale = 0.2


@window.event
def on_draw():
    global particle_sys
    # glClearColor(0.7, 0, 1, 1)
    glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)
    sprite.draw()
    particle_sys.add_particles()
    particle_sys.emit()


def main():
    pyglet.clock.schedule(update, 1 / 60)
    pyglet.app.run()


if __name__ == '__main__':
    main()
