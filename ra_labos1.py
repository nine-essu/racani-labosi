import copy
import sys

import numpy as np
from pyglet.gl import *
import math

p_kriv = []  # spremljene tocke krivulje
tang = []  # spremljene tocke tangenti
osi = []
kutovi = []
vrhovi_objekta = []
new_vert = []
polygons = []
i = 0
window = pyglet.window.Window()


@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60, float(width) / float(height), 0.1, 256)
    glMatrixMode(GL_MODELVIEW)
    glTranslated(0, 2.0, -7.0)  # kamera unazad da se vidi
    glRotated(45, 1, 0, 0)  # rotiramo kameru
    return True


@window.event
def on_draw():
    global p_kriv
    global tang
    global os
    global i

    glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)
    window.clear()
    nacrtaj_spiralu()
    nacrtaj_obj(p_kriv[i][0] / 10)
    i += 1
    if i > len(p_kriv) - 1:
        i = 0


def nacrtaj_spiralu():
    global p_kriv
    global tang
    global osi

    glColor3f(1, 1, 1)
    glBegin(GL_LINE_STRIP)
    for tocka in p_kriv:
        glVertex3f(tocka[0][0] / 10, tocka[0][1] / 10, tocka[0][2] / 10)
    glEnd()


def nacrtaj_obj(tocka_spirale):
    global p_kriv
    global tang
    global osi
    global kutovi
    global vrhovi_objekta
    global new_vert
    global polygons
    global i


    # translacija i rotacija objekta
    glPushMatrix()
    glTranslatef(tocka_spirale[0], tocka_spirale[1], tocka_spirale[2])
    glRotatef(kutovi[i], osi[i][0][0], osi[i][0][1], osi[i][0][2])
    glTranslatef(-tocka_spirale[0], -tocka_spirale[1], -tocka_spirale[2])

    new_vertices = []
    for vrh in vrhovi_objekta:
        diff = tocka_spirale - vrhovi_objekta[800]  # 1500 je nos za 747.obj, za neki drugi bi se trebalo promijenit
        new_vert = vrh + diff
        new_vertices.append(new_vert)

    # objekt
    for polygon in polygons:
        glBegin(GL_POLYGON)
        for idx in polygon:
            vertex = new_vertices[idx - 1]
            glColor3f(0.0, 1.0, 1.0)
            glVertex3f(vertex[0], vertex[1], vertex[2])
        glEnd()
    glPopMatrix()
    # sys.exit()

    # tangenta
    normalized_line = tang[i] / np.linalg.norm(tang[i])
    normalized_line += tocka_spirale

    glBegin(GL_LINE_STRIP)
    glColor3f(1, 0, 0)
    glVertex3f(tocka_spirale[0], tocka_spirale[1], tocka_spirale[2])
    glVertex3f(normalized_line[0][0], normalized_line[0][1], normalized_line[0][2])
    glEnd()


def update(*args):
    pass


def load_object():
    global polygons, vrhovi_objekta
    object_ = open("747.obj", "r")
    lines = object_.readlines()
    for line in lines:
        line = line.strip()
        if len(line) == 0 or line[0] in ("#", "g"):
            continue
        line = line.split(' ')
        if line[0] == "v":
            vertex = list(map(float, line[1:]))
            vrhovi_objekta += [vertex]
        elif line[0] == "f":
            polygon = list(map(int, line[1:]))
            polygons += [polygon]


def main():
    global p_kriv
    global tang
    global osi
    global kutovi
    global vrhovi_objekta
    global new_vert
    print()

    load_object()

    vrhovi_objekta = np.array(vrhovi_objekta)
    new_vert = copy.copy(vrhovi_objekta)  # nove tocke objekta koje ce se racunat za translaciju

    # vrhovi spirale iz pdf-a
    vrhovi = [[0, 0, 0], [0, 10, 5], [10, 10, 10], [10, 0, 15], [0, 0, 20], [0, 10, 25], [10, 10, 30], [10, 0, 35],
              [0, 0, 40], [0, 10, 45], [10, 10, 50], [10, 0, 55]]

    B = np.array([[-1, 3, -3, 1],
                  [3, -6, 3, 0],
                  [-3, 0, 3, 0],
                  [1, 4, 1, 0]])

    p_t = []  # tocke b-spline
    tang = []  # tocke tangente

    # segment (n-3)
    for i in range(len(vrhovi) - 3):
        v1 = vrhovi[i]
        v2 = vrhovi[i + 1]
        v3 = vrhovi[i + 2]
        v4 = vrhovi[i + 3]

        V = np.array([v1,
                      v2,
                      v3,
                      v4])

        # za svaki segment mijenjas t od 0 do 1
        t = 0
        s = np.array([0, 0, 1])  # pocetna orijentacija

        while t < 1:
            T = np.array([[t ** 3, t ** 2, t, 1]])
            rez = np.matmul(np.matmul(T * 1 / 6, B), V)
            p_t.append(rez)

            T = np.array([[3 * t ** 2, 2 * t, 1, 0]])
            e = np.matmul(np.matmul(T, B), V)
            tang.append(e)

            # e=tang (tj rez) je zeljena rotacija, neka pocetna bude s=(0, 0, 1)
            # os određuje os oko koje je potrebno rotirati objekt
            os = np.cross(s, e)
            osi.append(os)
            cos = (np.multiply(s, e)) / (np.linalg.norm(s) * np.linalg.norm(e))
            kut = math.acos(cos[0][2])
            kut = math.degrees(kut)
            kutovi.append(kut)
            t += 0.01

    p_kriv = copy.copy(p_t)

    pyglet.clock.schedule(update, .5)
    pyglet.app.run()


if __name__ == '__main__':
    main()
